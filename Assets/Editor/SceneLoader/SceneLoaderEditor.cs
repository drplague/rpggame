using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(SO_SceneLoader))]
public class SceneLoaderEditor : Editor
{
    SO_SceneLoader _sceneLoader;
    ReorderableList _sceneList;
    private void OnEnable()
    {
        _sceneLoader = (SO_SceneLoader)target;
        // Create a reference to the SceneLoader object.
        _sceneList = new ReorderableList(_sceneLoader.SceneCollection, typeof(SceneAsset), true, true, true, true);

    }
    public override void OnInspectorGUI()
    {
        // Draw header.
        _sceneList.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Scene Collection");
        };

        // List Remove button callback.
        _sceneList.onRemoveCallback = (ReorderableList _list) =>
        {
            _sceneLoader.SceneCollection.RemoveAt(_list.index);
            EditorUtility.SetDirty(_sceneLoader);
        };

        // Draw the scene list.
        _sceneList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            _sceneLoader.SceneCollection[index] = EditorGUI.ObjectField(rect, _sceneLoader.SceneCollection[index], typeof(SceneAsset), false) as SceneAsset;
        };

        // Add items to the list.
        _sceneList.onAddCallback = (ReorderableList _list) =>
        {
            _sceneLoader.SceneCollection.Add(default(SceneAsset));
            EditorUtility.SetDirty(_sceneLoader);
        };

        // Change scene order.
        _sceneList.onReorderCallback = (ReorderableList _list) =>
        {
            _sceneLoader.SceneCollection = _list.list as List<SceneAsset>;
            EditorUtility.SetDirty(_sceneLoader);
        };

        // Check if is able to remove any item.
        _sceneList.onCanRemoveCallback = (ReorderableList _list) =>
        {
            if (_list.count > 0 && _list.index >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        };

        // Remove selected item.
        _sceneList.onRemoveCallback = (ReorderableList _list) =>
        {
            _sceneLoader.SceneCollection.RemoveAt(_list.index);
            EditorUtility.SetDirty(_sceneLoader);
        };

        // Draw scene list.
        _sceneList.DoLayoutList();

        //Begin horizontal group.
        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Load to build"))
        {
            _sceneLoader.AddToBuild();
        }
        if (GUILayout.Button("Load Hierarchy"))
        {
            // Show should save changes window.
            if (!EditorUtility.DisplayDialog("Load Scenes", "Are you sure you want to load the scenes in the collection?", "Yes", "No"))
            {
                return;
            }
            EditorSceneManager.SaveOpenScenes();

            //Load collection to editor.
            int _sceneCount = _sceneLoader.SceneCollection.Count;
            string _scenePath = AssetDatabase.GetAssetPath(_sceneLoader.SceneCollection[0]);
            EditorSceneManager.OpenScene(_scenePath);

            for (int i = 1; i < _sceneCount; i++)
            {
                _scenePath = AssetDatabase.GetAssetPath(_sceneLoader.SceneCollection[i]);
                EditorSceneManager.OpenScene(_scenePath, OpenSceneMode.Additive);
            }
        }

        //End horizontal group.
        EditorGUILayout.EndHorizontal();

        // Clear list button.
        if (GUILayout.Button("Clear"))
        {
            _sceneLoader.SceneCollection.Clear();
        }
    }
    private void OnInspectorUpdate()
    {
        Repaint();
    }
}
