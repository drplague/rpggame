using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;
using UnityEngine;
using System;
using DialogEditor.Utils;
using DialogEditor.Data;
using DialogEditor.ScriptableObjects;
namespace DialogEditor
{

    public class DialogNode : Node
    {
        protected SO_Node _dialogNode;
        protected string _id;
        protected string _name;
        protected List<DialogChoiceSaveData> choices;
        protected string _text;
        protected DialogNodeTypes _dialogType;
        protected DialogGraphView _dgGraphView;

        public string ID
        {
            get => _id;
            set => _id = value;
        }
        public string Name
        {
            get => _name;
            set => _name = value;
        }
        public List<DialogChoiceSaveData> Choices
        {
            get => choices; 
            set => choices = value;
        }
        public string Text
        {
            get => _text;
            set => _text = value;
        }
        public DialogNodeTypes DialogType
        {
            get => _dialogType;
            set => _dialogType = value;
        }
        public DialogGraphView DgGraphView
        {
            get => _dgGraphView;
            set => _dgGraphView = value;
        }
        public virtual void Initialize(string nodeName, DialogGraphView dgView, Vector2 nodePosition)
        {
            //instatiate dialog node
            _dialogNode = ScriptableObject.CreateInstance<SO_Node>();
            _dialogNode.ID = Guid.NewGuid().ToString();
            _dialogNode.name = nodeName;
            Choices = new List<DialogChoiceSaveData>();
            _text = "Sample text.";

            SetPosition(new Rect(nodePosition, Vector2.zero));

            _dgGraphView = dgView;
        }
        public virtual void Draw()
        {
            TextField _dialogNameTextFiled = DialogElementUtils.CreateTextField(_name, null, null);

            titleContainer.Insert(0, _dialogNameTextFiled);
            
            // Create input port.
            Port inputPort = DialogElementUtils.CreatePort(this, "Input", Orientation.Horizontal, Direction.Input, Port.Capacity.Multi);

            inputContainer.Add(inputPort);

            // Create text filed.
            VisualElement _customDataContainer = new VisualElement();
            Foldout _textFoldout = DialogElementUtils.CreateFoldout("Dialog Txt: ");
            TextField _textField = DialogElementUtils.CreateTextArea("Sample dialog...");

            _textFoldout.Add(_textField);
            _customDataContainer.Add(_textFoldout);

            extensionContainer.Add(_customDataContainer);
        }
    }
}