namespace Roll
{
    public enum RollTestType
    {
        Strength,
        Dexterity,
        Intelligence,
        Appearance
    }
}