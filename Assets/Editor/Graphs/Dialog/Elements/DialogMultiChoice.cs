using UnityEngine;
using DialogEditor.Utils;
using UnityEngine.UIElements;
using UnityEditor.Experimental.GraphView;
using DialogEditor.Data;
using System;
namespace DialogEditor
{
    public class DialogMultiChoice : DialogNode
    {
        public override void Initialize(string nodeName,DialogGraphView dgView,Vector2 nodePosition){
            base.Initialize(nodeName,dgView,nodePosition);
            _dialogType = DialogNodeTypes.MultiChoice;

            DialogChoiceSaveData _newChoice = new DialogChoiceSaveData();
            
            _newChoice.Text = "New Choice";

            Choices.Add(_newChoice);
        }
        public override void Draw()
        {
            base.Draw();
            Button _addChoiceButton = DialogElementUtils.CreateButton("Add Choice",() => {
                DialogChoiceSaveData _choiceData = new DialogChoiceSaveData();
                _choiceData.Text = "Sample Dialog";
                Choices.Add(_choiceData);

                Port _choicePort = CreateChoicePort(_choiceData);

                outputContainer.Add(_choicePort);
            });

            mainContainer.Insert(1,_addChoiceButton);

            foreach(DialogChoiceSaveData _choice in Choices){
                Port _choicePort = CreateChoicePort(_choice);
                outputContainer.Add(_choicePort);
            }

            RefreshExpandedState();
        }
        private Port CreateChoicePort(object userData){
            Port _choicePort = DialogElementUtils.CreatePort(this);
            _choicePort.userData = userData;
            DialogChoiceSaveData _choiceData = userData as DialogChoiceSaveData;
            
            Button _deleteChoiceButton = DialogElementUtils.CreateButton("X",()=>{
                if(Choices.Count == 1){
                    return;
                }
                if(_choicePort.connected){
                    _dgGraphView.DeleteElements(_choicePort.connections);
                }

                Choices.Remove(_choiceData);
                _dgGraphView.RemoveElement(_choicePort);
            });
            
            TextField _choiceTextFiled = DialogElementUtils.CreateTextArea(_choiceData.Text,null,callback =>{
                _choiceData.Text = callback.newValue;
            });

            _choicePort.Add(_choiceTextFiled);
            _choicePort.Add(_deleteChoiceButton);
            
            return _choicePort;
        }
    }
}