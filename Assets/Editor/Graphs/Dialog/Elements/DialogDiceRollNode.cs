using UnityEngine;
using DialogEditor.Utils;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using DialogEditor.Data;
using Roll;

namespace DialogEditor
{
    public class DialogDiceRollNode : DialogNode
    {
        public override void Initialize(string nodeName, DialogGraphView dgView, Vector2 nodePosition)
        {
            base.Initialize(nodeName, dgView, nodePosition);

            DialogChoiceSaveData _choiceData = new DialogChoiceSaveData();
            _choiceData.Text = "Next Dialog";
            
            Choices.Add(_choiceData);
        }
        public override void Draw()
        {
            base.Draw();

            foreach(DialogChoiceSaveData _choice in Choices){
                Port _choicePort = DialogElementUtils.CreatePort(this);
                _choicePort.userData = _choice;
                outputContainer.Add(_choicePort);
            }
            
            EnumField _enumFiled = DialogElementUtils.CreateEnumFiled(RollTestType.Strength, "Roll Type",null);
            mainContainer.Add(_enumFiled);
            RefreshExpandedState();
        }
    }
}