using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DialogEditor
{
    public enum DialogNodeTypes
    {
        SingleChoice,
        MultiChoice,
        EventTrigger,
        DiceRoll
    }
}