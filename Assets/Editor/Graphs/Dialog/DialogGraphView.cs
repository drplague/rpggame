using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;
using DialogEditor.Utils;
namespace DialogEditor
{
    public class DialogGraphView : GraphView
    {
        List<DialogNode> _dialogNodes = new List<DialogNode>();
        public DialogGraphView()
        {
            AddGridBg();
            AddManipulators();
        }
        private void AddGridBg()
        {
            GridBackground _gridbg = new GridBackground();

            _gridbg.StretchToParentSize();

            Insert(0, _gridbg);
        }
        private void AddManipulators()
        {
            SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);

            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());

            // Choice buttons.
            this.AddManipulator(CreateNodeContextualMenu("Add Single Choice Node", DialogNodeTypes.SingleChoice));
            this.AddManipulator(CreateNodeContextualMenu("Add Multi Choice Node", DialogNodeTypes.MultiChoice));
            this.AddManipulator(CreateNodeContextualMenu("Add Trigger Node", DialogNodeTypes.EventTrigger));
            this.AddManipulator(CreateNodeContextualMenu("Add DiceRoll Node", DialogNodeTypes.DiceRoll));
        }
        public override List<Port> GetCompatiblePorts(Port sourcePort, NodeAdapter nodeAdapter)
        {
            List<Port> _compatiblePorts = new List<Port>();
            ports.ForEach(port =>
            {
                if ((sourcePort == port) || (sourcePort.node == port.node) || (sourcePort.direction == port.direction))
                    return;
                _compatiblePorts.Add(port);
            });
            return _compatiblePorts;
        }
        private IManipulator CreateNodeContextualMenu(string title, DialogNodeTypes nodeType)
        {
            // Create a new contextual menu for the node.
            ContextualMenuManipulator _contextualMenuManipulator = new ContextualMenuManipulator(
                menuEvent => menuEvent.menu.AppendAction(title, actionEvent => AddElement(CreateNode("DialogueName", nodeType, GetLocalMousePosition(actionEvent.eventInfo.localMousePosition))))
            );
            return _contextualMenuManipulator;
        }
        public DialogNode CreateNode(string dialogName, DialogNodeTypes dialogType, Vector2 position, bool draw = true)
        {
            // Create a new node.
            DialogNode _node = null;
            switch (dialogType)
            {
                case DialogNodeTypes.SingleChoice:
                    _node = new DialogSingleChoice();
                    break;
                case DialogNodeTypes.MultiChoice:
                    _node = new DialogMultiChoice();
                    break;
                case DialogNodeTypes.EventTrigger:
                    _node = new DialogEventNode();
                    break;
                case DialogNodeTypes.DiceRoll:
                    _node = new DialogDiceRollNode();
                    break;
            }

            //Initialize the node.
            _node.Initialize(dialogName,this,position);
            if(draw && _node != null)
                _node.Draw();
            return _node;
        }

        // Mouse click position.
        public Vector2 GetLocalMousePosition(Vector2 mousePosition)
        {
            Vector2 worldMousePosition = mousePosition;

            Vector2 localMousePosition = contentViewContainer.WorldToLocal(worldMousePosition);

            return localMousePosition;
        }
    }
}