using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using DialogEditor.ScriptableObjects;
using DialogEditor.Data;

namespace DialogEditor.Utils
{
    public static class DialogIOUtil
    {
        private static DialogGraphView _dialogGraph;
        private static string _fileName;
        private static string _contrainerFolderPath;
        private static List<DialogNode> _dialogNodes;
        //Initialize
        public static void Initialize(DialogGraphView dialogGraph, string graphName)
        {
            _dialogGraph = dialogGraph;
            _fileName = graphName;
            _contrainerFolderPath = $"Assets/Dialog/{_fileName}";
            _dialogNodes = new List<DialogNode>();
        }
        private static void CreateDefaultFolders()
        {
            CreateFolder("Assets/", "Dialog");
            CreateFolder("Assets/Dialog", _fileName);
        }
        public static void CreateFolder(string parentFolderPath, string newFolderName)
        {
            if (AssetDatabase.IsValidFolder($"{parentFolderPath}/{newFolderName}"))
            {
                return;
            }

            AssetDatabase.CreateFolder(parentFolderPath, newFolderName);
        }
        public static void Save()
        {
            CreateDefaultFolders();
            
            SO_DialogContainer _dialog = CreateAsset<SO_DialogContainer>(_contrainerFolderPath, _fileName);
            SaveDialogNodes();

        }
        private static void SaveDialogNodes()
        {
            foreach (DialogNode dialogNode in _dialogNodes)
            {
                
            }
        }
        public static T CreateAsset<T>(string path, string assetName) where T : ScriptableObject
        {
            T _asset = LoadAsset<T>(path, assetName);
            if (_asset == null)
            {
                _asset = ScriptableObject.CreateInstance<T>();
                AssetDatabase.CreateAsset(_asset, $"{path}/{assetName}.asset");
            }
            return _asset;
        }
        public static T LoadAsset<T>(string path, string assetName) where T : ScriptableObject
        {
            T _asset = AssetDatabase.LoadAssetAtPath<T>($"{path}/{assetName}.asset");
            return _asset;
        }
        public static void SaveNodeToScriptableObject(DialogNode _node, SO_DialogContainer _dialogContainer)
        {
            if (_dialogContainer.Dialogs == null)
            {
                _dialogContainer.Dialogs = new List<SO_Dialog>();
            }
        }
    }
}