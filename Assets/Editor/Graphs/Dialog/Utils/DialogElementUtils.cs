using System;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using Roll;
using GameEvents;
namespace DialogEditor.Utils
{
    public static class DialogElementUtils
    {
        public static Button CreateButton(string text, Action onClick = null)
        {
            Button _button = new Button(onClick);
            _button.text = text;
            return _button;
        }
        public static Foldout CreateFoldout(string title, bool collapsed = false)
        {
            Foldout _foldout = new Foldout();
            _foldout.text = title;
            _foldout.value = !collapsed;
            return _foldout;
        }
        public static Port CreatePort(DialogNode node, string portName = "Output", Orientation portOrientation = Orientation.Horizontal, Direction portDirection = Direction.Output, Port.Capacity portCapacity = Port.Capacity.Single)
        {
            Port _port = node.InstantiatePort(portOrientation, portDirection, portCapacity, typeof(bool));

            _port.portName = portName;

            return _port;
        }
        public static TextField CreateTextField(string fieldValue = "", string fieldLabel = "", EventCallback<ChangeEvent<string>> onValueChange = null)
        {
            TextField _textField = new TextField();

            _textField.value = fieldValue;
            _textField.label = fieldLabel;


            if (onValueChange != null)
            {
                _textField.RegisterValueChangedCallback(onValueChange);
            }

            return _textField;
        }
        public static TextField CreateTextArea(string areaValue = "", string areaLabel = "", EventCallback<ChangeEvent<string>> onValueChange = null)
        {
            TextField _areaTextField = CreateTextField(areaValue, areaLabel, onValueChange);

            _areaTextField.multiline = true;

            return _areaTextField;
        }
        public static ObjectField CreateObjectField(GameEvent eventTrigger = null)
        {
            ObjectField _objectField = new ObjectField();
            _objectField.objectType = typeof(GameEvent);
            _objectField.value = eventTrigger;
            return _objectField;
        }
        public static EnumField CreateEnumFiled(RollTestType fieldValue = RollTestType.Strength,string fieldLabel = "", EventCallback<ChangeEvent<Enum>> onValueChange = null)
        {
            EnumField _enumField = new EnumField(fieldValue);
            _enumField.label = fieldLabel;
            _enumField.RegisterValueChangedCallback(onValueChange);
            return _enumField;
        }
    }
}