using System;
using UnityEngine;
using DialogEditor.ScriptableObjects;
namespace DialogEditor.Data
{
    [Serializable]
    public class DialogChoiceData
    {
        [field: SerializeField] string _text;
        [field: SerializeField] SO_Dialog _nextDialog;
        public string Text { get => _text; set => _text = value; }
        public SO_Dialog NextDialog { get => _nextDialog; set => _nextDialog = value; }
    }
}