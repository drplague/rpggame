using System;
using System.Collections.Generic;
using UnityEngine;

namespace DialogEditor.Data
{
    [Serializable]
    public class DialogNodeSaveData
    {
        [field: SerializeField] string _id;
        [field: SerializeField] string _name;
        [field: SerializeField] string _text;
        [field: SerializeField] List<DialogChoiceSaveData> _choices;
        [field: SerializeField] DialogNodeTypes _dialogNodeType;
        [field: SerializeField] Vector2 _position;
        public string ID
        {
            get => _id;
            set => _id = value;
        }
        public string Name
        {
            get => _name;
            set => _name = value;
        }
        public string Text
        {
            get => _text;
            set => _text = value;
        }
        public List<DialogChoiceSaveData> Choices
        {
            get => _choices;
            set => _choices = value;
        }
        public DialogNodeTypes DialogNodeType
        {
            get => _dialogNodeType;
            set => _dialogNodeType = value;
        }
        public Vector2 Position
        {
            get => _position;
            set => _position = value;
        }
    }
}