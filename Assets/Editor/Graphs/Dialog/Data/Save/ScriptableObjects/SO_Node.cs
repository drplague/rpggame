using UnityEngine;
using System.Collections.Generic;

namespace DialogEditor.ScriptableObjects
{
    public class SO_Node : ScriptableObject
    {
        private string _id;
        private string _text;
        private List<SO_Node> _choices;
        private DialogNodeTypes _nodeType;
        private SO_Dialog _dialog;
        private Vector2 _position;

        public List<SO_Node> Choices { get => _choices; set => _choices = value; }
        public string ID { get => _id; set => _id = value; }
        public string Text { get => _text; set => _text = value; }
        public DialogNodeTypes NodeType { get => _nodeType; set => _nodeType = value; }
        public SO_Dialog Dialog { get => _dialog; set => _dialog = value; }
        public Vector2 Position { get => _position; set => _position = value; }
    }
}