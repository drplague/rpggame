using UnityEngine;
using System.Collections;
namespace DialogEditor.ScriptableObjects
{
    public class SO_Dialog : ScriptableObject
    {
        string _id;
        [field: SerializeField] string _dialogName;
        [field: SerializeField][field: TextArea()] string _text;
        [field: SerializeField] SO_Node _firstDialogNode;
        public string ID
        {
            get => _id;
            set => _id = value;
        }
        public string DialogName
        {
            get => _dialogName;
            set => _dialogName = value;
        }
        public string Text
        {
            get => _text;
            set => _text = value;
        }
        public SO_Node FirstDialogNode
        {
            get => _firstDialogNode;
            set => _firstDialogNode = value;
        }
    }
}