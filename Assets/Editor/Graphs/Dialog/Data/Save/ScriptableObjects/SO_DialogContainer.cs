using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DialogEditor.ScriptableObjects
{
    public class SO_DialogContainer : ScriptableObject
    {
        [field: SerializeField] string _fileName;
        [field: SerializeField] List<SO_Dialog> _dialogs;
        public void Initialize(string fileName)
        {
            _fileName = fileName;
        }
        public string FileName
        {
            get => _fileName;
            set => _fileName = value;
        }
        public List<SO_Dialog> Dialogs
        {
            get => _dialogs;
            set => _dialogs = value;
        }
    }
}