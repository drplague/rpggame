using System.Collections.Generic;
using DialogEditor.Data;
using UnityEngine;
namespace DialogEditor.ScriptableObjects
{
    public class SO_GraphSaveData : ScriptableObject
    {
        [field: SerializeField] string _fileName;
        [field: SerializeField] List<DialogNodeSaveData> _nodes;
        public void Initialize(string fileName)
        {
            _fileName = fileName;
            _nodes = new List<DialogNodeSaveData>();
        }
        public List<DialogNodeSaveData> Nodes
        {
            get => _nodes;
            set => _nodes = value;
        }
    }
}