using UnityEngine;
namespace DialogEditor.Data
{
    [System.Serializable]
    public class DialogChoiceSaveData
    {
        [field: SerializeField] private string _text;
        [field: SerializeField] private string _nodeId;
        public string NodeID
        {
            get => _nodeId;
            set => _nodeId = value;
        }
        public string Text
        {
            get => _text;
            set => _text = value;
        }
        public DialogChoiceSaveData(string id, string text)
        {
            _nodeId = id;
            _text = text;
        }
        public DialogChoiceSaveData()
        {
        }
    }
}