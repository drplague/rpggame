using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using DialogEditor.Utils;
namespace DialogEditor.Window
{
    public class DialogEditorWindow : EditorWindow
    {
        private DialogGraphView _dialogGraph;
        private static string _fileName = "Dialog File Name";
        private static TextField _fileNameTextFiled;
        private Button _saveButton;

        [MenuItem("Window/UI Toolkit/DialogWindow")]
        public static void Open()
        {
            DialogEditorWindow wnd = GetWindow<DialogEditorWindow>();
            wnd.titleContent = new GUIContent("DialogWindow");
        }
        private void OnEnable()
        {
            AddGraphView();
            AddToolBar();
        }
        private void AddGraphView()
        {
            DialogGraphView _graphView = new DialogGraphView();

            _graphView.StretchToParentSize();

            rootVisualElement.Add(_graphView);
        }

        // Toolbar.
        private void AddToolBar()
        {
            Toolbar _toolbar = new Toolbar();
            
            // File name text filed.
            _fileNameTextFiled = DialogElementUtils.CreateTextField(_fileName, "File Name:", callback =>
            {
                _fileNameTextFiled.value = callback.newValue;
            });
            
            // Save graph button.
            _saveButton = DialogElementUtils.CreateButton("Save", () =>
            {
                if (_fileNameTextFiled.value == "")
                {
                    return;
                }
                Save();
            });

            _toolbar.Add(_fileNameTextFiled);
            _toolbar.Add(_saveButton);
            rootVisualElement.Add(_toolbar);
        }
        private void Save()
        {
            // Initialize the dialog Save/Load utility.
            DialogIOUtil.Initialize(_dialogGraph,_fileNameTextFiled.value);
            DialogIOUtil.Save();
        }
    }
}