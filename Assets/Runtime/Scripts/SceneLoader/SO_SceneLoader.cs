using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "New Scene Collection Object", menuName = "New Scene Collection", order = 0)]
[System.Serializable]
public class SO_SceneLoader : ScriptableObject
{
    [SerializeField] SceneAsset _leveScene;
    [SerializeField] List<SceneAsset> _levelSceneCollection = new List<SceneAsset>();
    public List<SceneAsset> SceneCollection
    {
        get => _levelSceneCollection;
        set => _levelSceneCollection = value;
    }
    public void LoadCollection()
    {
        int _sceneCount = _levelSceneCollection.Count;

        //Load first level as main (in single mode).
        UnityEngine.SceneManagement.SceneManager.LoadScene(_levelSceneCollection[0].name, LoadSceneMode.Single);
        //Load all other levels (in additive mode).
        for (int i = 1; i < _sceneCount; i++)
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(_levelSceneCollection[i].name, LoadSceneMode.Additive);
        }
    }
    public void AddToBuild()
    {
        // Create list from array of build settings scenes.
        List<EditorBuildSettingsScene> _editorScenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);

        // List of scene paths in build settings.
        List<string> _scenePaths = new List<string>();

        _scenePaths = _editorScenes.Select(o => o.path).ToList();

        foreach (SceneAsset _scene in _levelSceneCollection)
        {
            string _scenePath = AssetDatabase.GetAssetPath(_scene);

            // If scene path not in build add it.
            if (!_scenePaths.Contains(_scenePath))
            {
                _editorScenes.Add(new EditorBuildSettingsScene(_scenePath, true));
            }
        }
        // Assign new array of scenes to build settings.
        EditorBuildSettings.scenes = _editorScenes.ToArray();
    }
}
