using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class PlayerCharacterController : MonoBehaviour
{
    [SerializeField] NavMeshAgent _agent;

    [SerializeField] private LayerMask _groundLayer;

    [SerializeField] private LayerMask _intractableLayer;

    [SerializeField] private Camera _mainCamera;

    private RaycastHit _raycastHit;

    private Ray _ray;

    private bool _goingToIntractable;

    private Vector3 _intractablePosition;
    private bool _isInteractionActive = false;

    void Update()
    {
        // Check if clicked on ui element.
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        // Check left mouse button to move.
        _ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButton(0) || Input.GetMouseButtonDown(0))
        {
            InteractionManager.InteractionFinish?.Invoke();
            if (!Physics.Raycast(_ray, out _raycastHit, Mathf.Infinity, _groundLayer))
            {
                return;
            }
            GoToPosition(_raycastHit.point);
            _goingToIntractable = false;
            _isInteractionActive = false;
        }

        // Check right mouse button to go to interaction point.
        if (Input.GetMouseButtonDown(1))
        {
            InteractionManager.InteractionFinish?.Invoke();
            _isInteractionActive = false;
            if (!Physics.Raycast(_ray, out _raycastHit, Mathf.Infinity, _intractableLayer))
            {
                _goingToIntractable = false;
                return;
            }
            foreach (Transform _child in _raycastHit.transform.GetComponentsInChildren<Transform>())
            {
                if (_child.tag == "InteractionPosition")
                {
                    _goingToIntractable = true;
                    _intractablePosition = _child.position;
                    GoToPosition(_child.position);
                    break;
                }
            }
        }

        // Check if player is near to interaction point.
        if ((_goingToIntractable == true) && (Vector3.Distance(_agent.transform.position, _intractablePosition) <= 0.2f) && (_isInteractionActive == false))
        {
            InteractionManager.InteractionBegin?.Invoke(_raycastHit.transform.gameObject);
            _isInteractionActive = true;
        }
    }

    private void GoToPosition(Vector3 position)
    {
        _agent.destination = position;
    }
}
