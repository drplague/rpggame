using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionManager : MonoBehaviour
{
    public delegate void InteractionShowDelegate(GameObject interactionObject);
    public static InteractionShowDelegate InteractionBegin;

    public delegate void InteractionHideDelegate();
    public static InteractionHideDelegate InteractionFinish;
    private void OnEnable()
    {
        InteractionBegin = null;
        InteractionFinish = null;
    }   
}
