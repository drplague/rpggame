using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ObjectPoolManager : MonoBehaviour
{
    [SerializeField] GameObject _prefab;
    [SerializeField][field: Range(2, 100)] int _size;
    [SerializeField] GameObject _parent;
    Queue<GameObject> _pool = new Queue<GameObject>();

    private void Start()
    {
        for (int i = 0; i < _size; i++)
        {
            GameObject _obj = Instantiate(_prefab);
            _obj.SetActive(false);
            if (_parent != null)
            {
                _obj.transform.SetParent(_parent.transform,false);
            }
            _pool.Enqueue(_obj);
        }
    }
    public GameObject PollObject()
    {
        GameObject _queueObject = _pool.Dequeue();
        _queueObject.SetActive(true);

        return _queueObject;

    }
    public void ReturnObjectToPool(GameObject returnedObject)
    {
        returnedObject.SetActive(false);
        _pool.Enqueue(returnedObject);
    }
}
