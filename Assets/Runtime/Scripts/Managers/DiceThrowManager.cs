using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceThrowManager : MonoBehaviour
{
    public delegate void ThrowDiceDelegate(ThrowData data);
    public static ThrowDiceDelegate ThrowDice;
    private void OnEnable()
    {
        ThrowDice = null;
    }
}
