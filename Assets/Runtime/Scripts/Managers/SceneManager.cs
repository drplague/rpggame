using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    // Scene Loader Delegate.
    public delegate void LoadSceneDelegate(SO_SceneLoader newSceneCollection);
    public static LoadSceneDelegate SceneCollectionLoader;
    private static SO_SceneLoader _currentSceneCollection;
    private void OnEnable()
    {
        SceneCollectionLoader += LoadNewScene;
    }
    private void OnDisable()
    {
        SceneCollectionLoader -= LoadNewScene;
    }
    private static void LoadNewScene(SO_SceneLoader newSceneCollection)
    {
        _currentSceneCollection = newSceneCollection;
        _currentSceneCollection.LoadCollection();
    }
}
