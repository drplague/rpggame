using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager : MonoBehaviour
{
    public delegate void EntityInfoDelegate(SO_Entity stats);
    public static EntityInfoDelegate EntityStats;
    private void OnEnable()
    {
        EntityStats = null;
    }
}
