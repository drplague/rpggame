using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoPopUpManager : MonoBehaviour
{
    public delegate void PopUpShowDelegate(string header, string description);
    public static PopUpShowDelegate PopUpShow;
    public delegate void PopUpHideDelegate();
    public static PopUpHideDelegate PopUpHide;
    private void OnEnable()
    {
        PopUpShow = null;
        PopUpHide = null;
    }
}
