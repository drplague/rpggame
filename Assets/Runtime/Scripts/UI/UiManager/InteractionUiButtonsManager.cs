using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script is attached to the InteractionUiButtons game object.
public class InteractionUiButtonsManager : MonoBehaviour
{
    [SerializeField] OverlayScript _overlayManager;
    [SerializeField] GameObject _interactButton;
    [SerializeField] GameObject _inspectButton;
    private void OnEnable()
    {
        if (_overlayManager.InteractionObject.TryGetComponent<Interaction>(out var interaction))
            _interactButton.SetActive(true);
        else
            _interactButton.SetActive(false);
        if (_overlayManager.InteractionObject.TryGetComponent<NpcScript>(out var npcScript))
            _inspectButton.SetActive(true);
        else
            _inspectButton.SetActive(false);
    }
    public void DoInteract()
    {
        _overlayManager.InteractionObject.GetComponent<Interaction>().Interact();
    }
    public void DoEntityInfoDisplay()
    {
        StatsManager.EntityStats?.Invoke(_overlayManager.InteractionObject.GetComponent<NpcScript>().NpcStats);
    }
}
