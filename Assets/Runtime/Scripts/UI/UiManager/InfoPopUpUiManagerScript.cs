using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class InfoPopUpUiManagerScript : MonoBehaviour
{
    [SerializeField] OverlayScript _overlayScript;
    [SerializeField] TMP_Text _headerText;
    [SerializeField] TMP_Text _descriptionText;
    [SerializeField] RectTransform _rectTransform;
    private void OnEnable()
    {
        _headerText.text = _overlayScript.PopupHeader;
        _descriptionText.text = _overlayScript.PopupDescription;
        CalculatePivot();
        _rectTransform.position = Input.mousePosition;
    }
    private void OnDisable()
    {
        _rectTransform.pivot = new Vector2(0, 1);
    }
    private void CalculatePivot()
    {
        Vector2 _mousePos = Input.mousePosition;
        Vector2 _newPivot = _rectTransform.pivot;

        // Calculate opposite corner in x axis.
        int _m = (_rectTransform.pivot.x == 0) ? 1 : -1;
        float _second_corner = _mousePos.x + (_m * _rectTransform.sizeDelta.x);

        // Check if opposite corner is outside of screen.
        if (_second_corner < 0 || _second_corner > Screen.width)
        {
            _newPivot.x = (_rectTransform.pivot.x == 0) ? 1 : 0;
        }

        // Calculate opposite corner in y axis.
        _m = (_rectTransform.pivot.x == 0) ? 1 : -1;
        _second_corner = _mousePos.y + (_m * _rectTransform.sizeDelta.y);

        // Check if opposite corner is outside of screen.
        if (_second_corner < 0 || _second_corner > Screen.height)
        {
            _newPivot.y = (_rectTransform.pivot.y == 0) ? 1 : 0;
        }

        _rectTransform.pivot = _newPivot;
    }
}
