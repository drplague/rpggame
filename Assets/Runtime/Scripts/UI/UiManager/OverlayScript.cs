using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script attached to Overlay GameObject.
public class OverlayScript : MonoBehaviour
{
    [SerializeField] GameObject _interactionUI;

    [SerializeField] GameObject _diceThrowUI;

    [SerializeField] GameObject _statsUI;
    [SerializeField] GameObject _popupUI;
    [SerializeField] ObjectPoolManager _attributesPool;
    GameObject _interactionObject;
    ThrowData _throwData;
    SO_Entity _entityData;
    string _popupHeader;
    string _popupDescription;
    public GameObject InteractionObject
    {
        get => _interactionObject;
    }
    public ThrowData ThrowData
    {
        get => _throwData;
    }
    public SO_Entity EntityData
    {
        get => _entityData;
    }
    public ObjectPoolManager AttributesPool
    {
        get => _attributesPool;
    }
    public string PopupHeader
    {
        get => _popupHeader;
    }
    public string PopupDescription
    {
        get => _popupDescription;
    }

    void OnEnable()
    {
        InteractionManager.InteractionBegin += EnableInteractionUi;
        StatsManager.EntityStats += EnableStatsInfoUi;
        DiceThrowManager.ThrowDice += EnableDiceThrowUi;
        InteractionManager.InteractionFinish += HideUi;
        InfoPopUpManager.PopUpShow += ShowPopUp;
        InfoPopUpManager.PopUpHide += HidePopUp;
    }
    void OnDisable()
    {
        InteractionManager.InteractionBegin -= EnableInteractionUi;
        StatsManager.EntityStats -= EnableStatsInfoUi;
        DiceThrowManager.ThrowDice -= EnableDiceThrowUi;
        InteractionManager.InteractionFinish -= HideUi;
        InteractionManager.InteractionFinish += HidePopUp;
        InfoPopUpManager.PopUpShow -= ShowPopUp;
        InfoPopUpManager.PopUpHide -= HidePopUp;

    }
    private void EnableInteractionUi(GameObject interactionObject)
    {
        _interactionObject = interactionObject;
        _interactionUI.SetActive(true);
        _diceThrowUI.SetActive(false);
        _statsUI.SetActive(false);
    }
    private void EnableDiceThrowUi(ThrowData throwData)
    {
        _throwData = throwData;
        _interactionUI.SetActive(false);
        _diceThrowUI.SetActive(true);
        _statsUI.SetActive(false);
    }
    private void EnableStatsInfoUi(SO_Entity data)
    {
        _entityData = data;
        _interactionUI.SetActive(false);
        _diceThrowUI.SetActive(false);
        _statsUI.SetActive(true);
    }
    private void HideUi()
    {
        _interactionUI.SetActive(false);
        _diceThrowUI.SetActive(false);
        _statsUI.SetActive(false);
    }
    private void ShowPopUp(string header, string content)
    {
        _popupHeader = header;
        _popupDescription = content;
        _popupUI.SetActive(true);
    }
    private void HidePopUp()
    {
        _popupUI.SetActive(false);
    }
}
