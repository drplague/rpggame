using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ThrowUiScript : MonoBehaviour
{
    [SerializeField] OverlayScript _overlayManager;
    [SerializeField] TMP_Text _throwScore;
    [SerializeField] TMP_Text _failText;
    private void Start()
    {
        _throwScore.text = _overlayManager.ThrowData.Score.ToString();
        if (_overlayManager.ThrowData.Success)
            _failText.text = "Success";
        else
            _failText.text = "Fail";
    }
}
