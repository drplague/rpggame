using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class InteractionUiScript : MonoBehaviour
{
    [SerializeField]
    GameObject _interactionUi;

    private void OnEnable()
    {
        InteractionManager.InteractionBegin += ShowUi;
        InteractionManager.InteractionFinish += HideUi;
    }

    private void OnDisable()
    {
        InteractionManager.InteractionBegin -= ShowUi;
        InteractionManager.InteractionFinish -= HideUi;
    }

    private async void ShowUi(GameObject interactionObject)
    {
        float _remainingTime = 1f;
        while (_remainingTime > 0)
        {
            _interactionUi.transform.position = Camera.main.WorldToScreenPoint(
                interactionObject.transform.position
            );
            _remainingTime -= Time.deltaTime;
            await Task.Yield();
        }
    }

    private void HideUi()
    {
        _interactionUi.SetActive(false);
    }
}
