using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Threading.Tasks;

public class AttributesUiScript : MonoBehaviour
{
    [SerializeField] SO_Attribute _attribute;
    [SerializeField] Image _attributeImage;

    public SO_Attribute Attribute
    {
        get => _attribute;
        set
        {
            _attribute = value;
            Refresh();
        }
    }
    private void Refresh()
    {
        _attributeImage.sprite = _attribute.AttributeIcon;
    }
    public async void OnMouseEnter()
    {
        await Task.Delay(500);
        InfoPopUpManager.PopUpShow?.Invoke(_attribute.AttributeName, _attribute.AttributeDescription);
    }
    public void OnMouseExit()
    {
        InfoPopUpManager.PopUpHide?.Invoke();
    }
}
