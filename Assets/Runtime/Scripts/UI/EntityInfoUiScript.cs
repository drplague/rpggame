using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EntityInfoUiScript : MonoBehaviour
{
    [SerializeField] OverlayScript _overlayManager;
    SO_Entity _entityStats;
    [SerializeField] private TMP_Text _nameText;
    [SerializeField] private TMP_Text _occupationText;
    [SerializeField] private TMP_Text _degreeText;
    [SerializeField] private TMP_Text _ageText;
    [SerializeField] private TMP_Text _genderText;
    [SerializeField] private GameObject _attributesPanel;
    private void OnEnable()
    {
        _entityStats = _overlayManager.EntityData;
        _nameText.text = _entityStats.Name;
        _degreeText.text = _entityStats.Occupation.OccupationName;
        _ageText.text = _entityStats.Age.ToString();
        _genderText.text = _entityStats.Gender.ToString();
        int _attrCount = _entityStats.SecondaryAttributes.Count;

        foreach (SO_Attribute _attribute in _entityStats.SecondaryAttributes)
        {
            GameObject _attrButton = _overlayManager.AttributesPool.PollObject();
            _attrButton.GetComponent<AttributesUiScript>().Attribute = _attribute;
        }
    }
    // TODO: Poll object and return
    private void OnDisable()
    {
        foreach (Transform _attrButton in _attributesPanel.transform)
        {
            _overlayManager.AttributesPool.ReturnObjectToPool(_attrButton.gameObject);
        }
    }
}
