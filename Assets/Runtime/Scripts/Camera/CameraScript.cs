using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CameraScript : MonoBehaviour
{
    private Vector3 _offset;
    [SerializeField] private Transform _target;
    [SerializeField] private float _smoothingTime;
    private Vector3 _velocity = Vector3.zero;
    private void Awake(){
        _offset = transform.position - _target.position;
    }
    private void Update(){
        transform.position = Vector3.SmoothDamp(transform.position,_target.position + _offset,ref _velocity,_smoothingTime);
    }
}
