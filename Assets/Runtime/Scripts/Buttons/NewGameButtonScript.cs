using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGameButtonScript : MonoBehaviour
{
   [SerializeField] SO_SceneLoader _firstFloorCollection;
   public void StartGame(){
        SceneManager.SceneCollectionLoader?.Invoke(_firstFloorCollection);
    }
}