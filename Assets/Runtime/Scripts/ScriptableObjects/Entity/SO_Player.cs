using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "NPC", menuName = "Entity/Player")]
public class SO_Player : SO_Entity
{
    [SerializeField] [field: Range(1,5)] float _speed;
}
