using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public abstract class SO_Entity : ScriptableObject
{
    //Primary stats
    [SerializeField] string _name;
    [SerializeField] SO_Occupation _occupation;
    [SerializeField] GenderEnum _gender;
    [SerializeField][Range(15, 90)] int _age;
    [SerializeField][Range(0, 10)] protected int _strength;
    [SerializeField][Range(0, 10)] protected int _dexterity;
    [SerializeField][Range(0, 10)] protected int _intelligence;
    [SerializeField][Range(0, 10)] protected int _appearance;
    [SerializeField] List<SO_Attribute> _secondaryAttributes;
    public string Name
    {
        get => _name;
    }
    public SO_Occupation Occupation
    {
        get => _occupation;
    }
    public GenderEnum Gender
    {
        get => _gender;
    }
    public int Age
    {
        get => _age;
    }
    public int Strength
    {
        get => _strength;
    }
    public int Dexterity
    {
        get => _dexterity;
    }
    public int Intelligence
    {
        get => _intelligence;
    }
    public int Appearance
    {
        get => _appearance;
    }
    public List<SO_Attribute> SecondaryAttributes
    {
        get => _occupation.SecondaryAttributes.Concat(_secondaryAttributes).ToList();
    }
}