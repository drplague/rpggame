using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Attribute", menuName = "Entity/Attributes")]
public class SO_Attribute : ScriptableObject
{
    [SerializeField] string _attributeName;
    [SerializeField] Sprite _attributeIcon = null;
    [SerializeField][field: Range(0, 100)] int _attributeValue;
    [SerializeField][field: TextArea] string _attributeDescription;
    [SerializeField] BaseStats _baseStatEnum;
    public string AttributeName
    {
        get => _attributeName;
    }
    public float AttributeValue
    {
        get => _attributeValue / 100;
    }
    public BaseStats BaseStatEnum
    {
        get => _baseStatEnum;
    }
    public Sprite AttributeIcon
    {
        get => _attributeIcon;
    }
    public string AttributeDescription
    {
        get => _attributeDescription;
    }
}
