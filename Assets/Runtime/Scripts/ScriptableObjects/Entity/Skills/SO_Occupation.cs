using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu(fileName = "Occupation", menuName = "Entity/Occupation")]
public class SO_Occupation : ScriptableObject
{
    [SerializeField] string _occupationName;
    [SerializeField] Image _occupationImage;
    [SerializeField] List<SO_Attribute> _secondaryAttributes;

    public string OccupationName
    {
        get => _occupationName;
    }
    public List<SO_Attribute> SecondaryAttributes
    {
        get => _secondaryAttributes;
    }
}