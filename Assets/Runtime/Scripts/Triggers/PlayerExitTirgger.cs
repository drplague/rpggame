using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;
public class PlayerExitTirgger : MonoBehaviour
{
    [SerializeField] GameEvent exitEvent;
    void OnCollisionExit(Collision other) => ExitCheck(other.collider);
    void OnTriggerExit(Collider other) => ExitCheck(other);
    void ExitCheck(Collider other)
    {
        if (other.tag == "Player")
        {
            exitEvent?.Invoke();
        }
    }
}
