using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToOtherLevel : Interaction
{
    [SerializeField] SO_SceneLoader _nextSceneCollection;

    public override void Interact()
    {
        SceneManager.SceneCollectionLoader?.Invoke(_nextSceneCollection);
    }
}
