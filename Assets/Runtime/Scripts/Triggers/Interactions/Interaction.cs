using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interaction : MonoBehaviour
{
    public abstract void Interact();
    protected bool _isActive = true;
    public bool IsActive
    {
        get => _isActive;
    }
}
