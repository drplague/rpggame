using System.Threading.Tasks;
using UnityEngine;
public class DoorsDiceRollTest : TestInteraction
{
    [SerializeField] protected Animator _doorsAnimation;

    protected override void TestSuccess()
    {
        UnlockDoors(1000);
    }
    protected override void TestFail()
    {
        _doorsAnimation.SetTrigger("TriggerLocked");
    }
    private async void UnlockDoors(int delay)
    {
        await Task.Delay(delay);
        _doorsAnimation.SetTrigger("TriggerOpen");
    }
}