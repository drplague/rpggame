using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;
public class PlayerEnterTirgger : MonoBehaviour
{
    [SerializeField] GameEvent enterEvent;
    void OnCollisionEnter(Collision other) => EnterCheck(other.collider);
    void OnTriggerEnter(Collider other) => EnterCheck(other);
    void EnterCheck(Collider other){
        if(other.tag == "Player"){
            enterEvent?.Invoke();
        }
    }
}
