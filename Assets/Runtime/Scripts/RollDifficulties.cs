public enum Difficulties
{
    Trivial,
    Easy,
    Medium,
    Hard,
    VeryHard,
    Impossible,
}