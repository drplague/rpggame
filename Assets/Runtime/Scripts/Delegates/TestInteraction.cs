using UnityEngine;
public abstract class TestInteraction : Interaction
{
    [SerializeField] protected BaseStats _rollType;
    [SerializeField] protected SO_Dice _dice;
    [SerializeField] protected Difficulties _difficulty;
    protected ThrowData _throwData;
    public override void Interact()
    {
        _throwData = new ThrowData(_dice, _rollType, 2, _difficulty);
        if (_isActive)
        {
            DiceThrowManager.ThrowDice?.Invoke(_throwData);
            if (_throwData.Success)
            {
                TestSuccess();
            }
            else
            {
                TestFail();
            }
        }
        else
        {
            TestFail();
        }
        _isActive = false;
    }
    protected abstract void TestSuccess();
    protected abstract void TestFail();
}