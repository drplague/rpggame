using UnityEngine;

[CreateAssetMenu(fileName = "New Dice", menuName = "RpgGame/Dice", order = 0)]
public class SO_Dice : ScriptableObject {
    [SerializeField] [field: Range(1,20)] int _sidesCount;
    [SerializeField] GameObject _diceMesh;
    [SerializeField] Animator _animation;
    public int SideCount{
        get => _sidesCount;
    }
}