using UnityEngine;

public class ThrowData
{
    int _score;
    SO_Dice _dice;
    BaseStats _rollTestType;
    bool _success;
    int _difficulty;
    int _modifier;
    public ThrowData(SO_Dice dice, BaseStats rollTestType, int modifiers, Difficulties difficulty)
    {
        _dice = dice;
        _rollTestType = rollTestType;
        switch (difficulty)
        {
            case Difficulties.Trivial:
                _difficulty = 5;
                break;
            case Difficulties.Easy:
                _difficulty = 10;
                break;
            case Difficulties.Medium:
                _difficulty = 15;
                break;
            case Difficulties.Hard:
                _difficulty = 20;
                break;
            case Difficulties.VeryHard:
                _difficulty = 25;
                break;
            case Difficulties.Impossible:
                _difficulty = 30;
                break;
        }        
        // Roll random number.
        _score = UnityEngine.Random.Range(1, _dice.SideCount);
        // Check if success.
        if (_score + modifiers >= _difficulty)
        {
            _success = true;
        }
        else
        {
            _success = false;
        }
    }

    public int Score
    {
        get => _score;
    }
    public SO_Dice Dice
    {
        get => _dice;
        set => _dice = value;
    }
    public BaseStats RollTestType
    {
        get => _rollTestType;
        set => _rollTestType = value;
    }
    public bool Success
    {
        get => _success;
    }
    public int Difficulty
    {
        get => _difficulty;
    }
    public int Modifier
    {
        get => _modifier;
    }
}