using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcScript : MonoBehaviour
{
    [SerializeField]
    SO_Npc _npcStats;
    public SO_Npc NpcStats
    {
        get => _npcStats;
    }
}